import brand from '../../assets/image/brand.png'
import './Navbar.css'
import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { Link, withRouter } from 'react-router-dom'
import UserNavbar from './UserNavbar'

class Navbar extends Component {
    render() {
        return (
            <nav className="navbar sticky-top navbar-expand-lg navbar-light bg-light">
                <Link className="navbar-brand" to="/"><img alt="cocreate-logo" src={brand} /></Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        {!this.props.userIsLoggedIn && this.props.location.pathname === '/' && (
                            <Fragment>
                                <li className="nav-item">
                                    <button onClick={() => { document.querySelector('#fitur').scrollIntoView({ behavior: 'smooth' }) }} className="btn btn-nav-link">Fitur</button>
                                </li>
                                <li className="nav-item">
                                    <button onClick={() => { document.querySelector('#discover').scrollIntoView({ behavior: 'smooth' }) }} className="btn btn-nav-link">Jelajahi</button>
                                </li>
                                <li className="nav-item">
                                    <button onClick={() => { document.querySelector('#our-team').scrollIntoView({ behavior: 'smooth' }) }} className="btn btn-nav-link">Tim Kami</button>
                                </li>
                            </Fragment>
                        )}
                    </ul>
                    <ul className="navbar-nav">
                        {this.props.userIsLoggedIn ?
                            <UserNavbar /> :
                            <Link to="/login" role="button" className="btn btn-navbar-login">Login</Link>
                        }
                    </ul>
                </div>
            </nav>
        )
    }
}

const _mapStatetoProps = state => {
    return {
        userIsLoggedIn: state.userIsLoggedIn,
        user: state.user
    }
}

const _mapDispatchToProps = dispatch => {
    return {
        dispatch
    }
}

export default connect(_mapStatetoProps, _mapDispatchToProps)(withRouter(Navbar))
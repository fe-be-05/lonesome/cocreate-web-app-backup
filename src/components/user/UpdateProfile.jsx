import React, { Component } from 'react'
import { connect } from 'react-redux'
import { reactLocalStorage } from 'reactjs-localstorage'
import SimpleModal from '../modal/SimpleModal'

class UpdateProfile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: this.props.user,
        }

        this._handleGenderChange = this._handleGenderChange.bind(this)
        this._handleProfileChange = this._handleProfileChange.bind(this)
    }

    _handleGenderChange(e) {
        this.setState(ps => ({user: {...ps.user, gender: e.target.value}}))
    }

    async _handleProfileChange(event) {
        event.preventDefault()

        var myHeaders = new Headers()
        myHeaders.append("Content-Type", "application/json")
        myHeaders.append("Authorization", this.props.user.token)

        var raw = JSON.stringify({
            ...this.state.user
        })

        const response = await fetch("http://kelompok5.dtstakelompok1.com/account/update", {
            method: 'PUT',
            headers: myHeaders,
            body: raw,
            redirect: "follow"
        })
            .then(response => response.json())
            .catch(error => console.log('Error: ', error))
        if (response == null) return null
        if (response.success) {
            this.props.dispatch({
                type: "LOGIN",
                payload: {
                    user: this.state.user,
                }
            })
            if (reactLocalStorage.get('rememberMe', false)) reactLocalStorage.setObject('user', this.state.user)
            window.$('#profileUpdateModal').modal('show')
        } else {
            alert(`Error: ${response.error}`)
        }
    }

    render() {
        return (
            <form onSubmit={this._handleProfileChange} className="form-profile">
                <div className="form-group">
                    <label htmlFor="input-name">Nama:</label>
                    <input onChange={(e) => this.setState(ps => ({user: {...ps.user, name: e.target.value}}))} type="text" className="form-control" value={this.state.user.name} id="input-name" />
                </div>
                <div className="form-group">
                    <label htmlFor="input-email">Email:</label>
                    <input type="email" className="form-control" value={this.state.user.email} id="input-email" disabled />
                </div>
                <div className="my-2">
                    Jenis kelamin:
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="gender" id="radio-laki-laki" value="Laki-laki" checked={this.state.user.gender === 'Laki-laki'} onChange={this._handleGenderChange} />
                        <label className="form-check-label" htmlFor="radio-laki-laki">Laki-laki</label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="gender" id="radio-perempuan" value="Perempuan" checked={this.state.user.gender === 'Perempuan'} onChange={this._handleGenderChange} />
                        <label className="form-check-label" htmlFor="radio-perempuan">Perempuan</label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input" type="radio" name="gender" id="radio-lainnya" value="Lainnya" checked={this.state.user.gender === 'Lainnya'} onChange={this._handleGenderChange} />
                        <label className="form-check-label" htmlFor="radio-lainnya">Lainnya</label>
                    </div>
                </div>
                <div className="form-group">
                    <label htmlFor="input-name">Tempat Lahir:</label>
                    <input onChange={(e) => this.setState(ps => ({user: {...ps.user, place_of_birth: e.target.value}}))} type="text" className="form-control" value={this.state.user.place_of_birth} id="input-name" />
                </div>
                <div className="form-group">
                    <label htmlFor="input-name">Tanggal Lahir:</label>
                    <input onChange={(e) => this.setState(ps => ({user: {...ps.user, birthday: e.target.value}}))} type="date" className="form-control" value={this.state.user.birthday} id="input-name" />
                </div>
                <div className="form-group">
                    <label htmlFor="input-name">Pekerjaan:</label>
                    <input onChange={(e) => this.setState(ps => ({user: {...ps.user, profession: e.target.value}}))} type="text" className="form-control" value={this.state.user.profession} id="input-name" />
                </div>
                <div className="form-group">
                    <label htmlFor="input-name">Akun Gitlab:</label>
                    <input onChange={(e) => this.setState(ps => ({user: {...ps.user, gitlab: e.target.value}}))} type="text" className="form-control" value={this.state.user.gitlab} id="input-name" />
                </div>
                <div className="form-group">
                    <label htmlFor="input-name">Akun Github:</label>
                    <input onChange={(e) => this.setState(ps => ({user: {...ps.user, github: e.target.value}}))} type="text" className="form-control" value={this.state.user.github} id="input-name" />
                </div>
                <div className="form-group">
                    <label htmlFor="input-name">Akun LinkedIn:</label>
                    <input onChange={(e) => this.setState(ps => ({user: {...ps.user, linkedin: e.target.value}}))} type="text" className="form-control" value={this.state.user.linkedin} id="input-name" />
                </div>
                <div className="form-group">
                    <label htmlFor="input-name">Nomor Telepon:</label>
                    <input onChange={(e) => this.setState(ps => ({user: {...ps.user, handphone: e.target.value}}))} type="text" className="form-control" value={this.state.user.handphone} id="input-name" />
                </div>
                <div className="form-group">
                    <label htmlFor="input-alamat">Alamat:</label>
                    <textarea onChange={(e) => this.setState(ps => ({user: {...ps.user, address: e.target.value}}))} className="form-control" value={this.state.user.address} id="input-alamat" rows="5"></textarea>
                </div>
                <button className="btn btn-app mr-2" onClick={this._handleProfileChange}>Simpan</button>

                <SimpleModal modalID='profileUpdateModal' modalTitle="Sukses" modalBody="Profil Anda berhasil diperbarui" />
            </form>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.user,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        dispatch
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdateProfile)
import React, { Component } from 'react'
import './ProfileView.css'
import LoadingScreen from '../loading/index'
import ErrorPage from '../error/Error404'
import ProfilePictureDefault from '../../assets/image/profile.png'

// laman detail user yang diakses orang lain
export default class ProfileView extends Component {
    constructor(props) {
        super(props)
        this.state = {
            userID: this.props.match.params.user_id,
            user: {},
            hasHitAPI: false,
            foundUser: true,
        }
    }
    componentDidMount() {
        this._getUser(this.state.userID)
    }

    // get user profile
    async _getUser(userID) {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow',
        };

        const r = await fetch(`http://kelompok5.dtstakelompok1.com/account/find/${userID}`, requestOptions)
            .then(response => response.json())
            .catch(error => {
                console.log('Error: ', error)
                alert(`Error: ${error}`)
            })

        if (r == null) return null
        if (r.success) {
            this.setState({
                hasHitAPI: true,
                user: r.account,
                foundUser: true,
            })
        } else {
            this.setState({
                foundUser: false,
            })
        }
    }

    render() {
        if (!this.state.hasHitAPI) return <LoadingScreen />
        if (!this.state.foundUser) return <ErrorPage />
        return (
            <div className="container-fluid px-0">
                <div className="text-center profile-header">
                    <img className="rounded-circle profile-photo" alt="profil" src={this.state.user.photo || ProfilePictureDefault} />
                    <h1>{this.state.user.name}</h1>
                </div>
                <div className="container my-3">
                    <dl className="row">
                        <dt className="col-md-3">Jenis kelamin</dt>
                        <dd className="col-md-9">{this.state.user.gender || '-'}</dd>

                        <dt className="col-md-3">Nomor telepon</dt>
                        <dd className="col-md-9">{this.state.user.handphone || '-'}</dd>

                        <dt className="col-md-3">Alamat</dt>
                        <dd className="col-md-9">{this.state.user.address || '-'}</dd>

                        <dt className="col-md-3">Email</dt>
                        <dd className="col-md-9">{this.state.user.email}</dd>

                        <dt className="col-md-3">Pekerjaan</dt>
                        <dd className="col-md-9">{this.state.user.profession || '-'}</dd>

                        <dt className="col-md-3">URL LinkedIn</dt>
                        <dd className="col-md-9">{this.state.user.linkedin || '-'}</dd>

                        <dt className="col-md-3">URL Github</dt>
                        <dd className="col-md-9">{this.state.user.github || '-'}</dd>

                        <dt className="col-md-3">URL Gitlab</dt>
                        <dd className="col-md-9">{this.state.user.gitlab || '-'}</dd>
                    </dl>
                </div>
            </div>
        )
    }
}

import React, { Component } from 'react'
import './UpdateCategory.css'
import LoadingScreen from '../loading/index'
import { connect } from 'react-redux'
import SimpleModal from '../modal/SimpleModal'
import { reactLocalStorage } from 'reactjs-localstorage'

class UpdateCategory extends Component {
    constructor(props) {
        super(props)
        this.state = {
            availableCategories: [],
            availableCategoriesHtml: [],
            selectedCategories: this.props.user.category_id.split(','),
            hasHitAPI: false,
        }
        this._getAllCategories()
    }

    componentDidMount() {
        console.log('u cat', this.props.user.category_id)
    }

    _handleSelectCategories(e) {
        var selectedCategories = this.state.selectedCategories
        if (!selectedCategories.includes(e.target.value)) {
            selectedCategories.push(e.target.value)
        } else {
            const index = selectedCategories.indexOf(e.target.value)
            if (index > -1) {
                selectedCategories.splice(index, 1)
            }
        }
        this.setState({ selectedCategories: selectedCategories })
    }

    async _handleSaveCategories(e) {
        e.preventDefault()
        const selectedCategoriesAsString = this.state.selectedCategories.join(',')
        var myHeaders = new Headers()
        myHeaders.append("Content-Type", "application/json")
        myHeaders.append("Authorization", this.props.user.token)

        var raw = JSON.stringify({ "category_id": selectedCategoriesAsString })

        var requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: raw,
            redirect: 'follow',
        }

        const r = await fetch("http://kelompok5.dtstakelompok1.com/account/update", requestOptions)
            .then(response => response.json())
            .catch(error => {
                console.log('Error: ', error)
                alert(`Error: ${error}`)
            })

        if (r == null) return null
        if (r.success) {
            var user = this.props.user
            user = { ...user, category_id: selectedCategoriesAsString }
            this.props.dispatch({
                type: "LOGIN",
                payload: {
                    user: user,
                }
            })
            if (reactLocalStorage.get('rememberMe', false)) reactLocalStorage.setObject('user', user)
            window.$('#profileCategoryUpdateModal').modal('show')
        }
    }

    async _getAllCategories() {
        const response = await fetch('http://kelompok5.dtstakelompok1.com/category')
            .then(resp => resp.json())
            .catch(error => {
                console.log('Error: ', error)
                alert(`Error: ${error}`)
            })
        if (response == null) return null
        if (response.success) {
            this.setState({ availableCategories: response.account, hasHitAPI: true })
        }
    }

    _renderCategories() {
        return this.state.availableCategories.map((item) => {
            return (
                <div class="form-check">
                    <input onChange={this._handleSelectCategories.bind(this)} className="form-check-input" type="checkbox" name={item.category_name} id={`category-${item.id}`} value={item.id} checked={this.state.selectedCategories.includes(item.id.toString())} />
                    <label className="form-check-label" htmlFor={`category-${item.id}`}>{item.category_name}</label>
                </div>
            )
        })
    }

    render() {
        if (!this.state.hasHitAPI) return <LoadingScreen />
        return (
            <form className="form-profile" onSubmit={this._handleSaveCategories.bind(this)}>
                <h4>Kategori Favorit Anda</h4>
                <div className="form-group">
                    {this._renderCategories()}
                </div>
                <button onClick={this._handleSaveCategories.bind(this)} className="btn btn-app">Simpan</button>

                <SimpleModal modalID='profileCategoryUpdateModal' modalTitle="Sukses" modalBody="Profil Anda berhasil diperbarui" />
            </form>
        )
    }
}

function _mapStateToProps(state) {
    return {
        user: state.user,
    }
}

const _mapDispatchToProps = dispatch => {
    return {
        dispatch
    }
}

export default connect(_mapStateToProps, _mapDispatchToProps)(UpdateCategory)
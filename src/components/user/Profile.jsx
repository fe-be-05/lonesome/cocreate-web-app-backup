import React, { Component } from 'react'
import { connect } from 'react-redux'
import UpdateProfile from './UpdateProfile'
import UpdatePhoto from './UpdatePhoto'
import ChangePassword from './ChangePassword'
import './Profile.css'
import LazyLoad from 'react-lazyload'
import DefaultProfileImage from '../../assets/image/profile.png'
import UpdateCategory from './UpdateCategory'

class Profile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            editMode: 'profile', // also 'photo' and 'password'
        }
    }

    componentDidMount() {
        // by default, it is profile-edit mode
        this.setState({ editMode: 'profile' })
    }

    _handleTabChange(event, tabName) {
        event.preventDefault()
        switch (tabName) {
            case 'edit-profile':
                this.setState({ editMode: 'profile' })
                break
            case 'change-photo':
                this.setState({ editMode: 'photo' })
                break
            case 'change-password':
                this.setState({ editMode: 'password' })
                break
            case 'kategori':
                this.setState({ editMode: 'kategori' })
                break
            default:
                break
        }
    }

    render() {
        return (
            <div className="container">
                {!this.props.user.emailverification && <div class="p-3 mb-2 bg-danger text-white">Anda belum melakukan verifikasi email.</div>}
                <LazyLoad height={192}><img src={this.props.user.photo || DefaultProfileImage} alt="foto-profil" className="rounded-circle img-profile" /></LazyLoad>
                <h2 className="text-center my-2">{this.props.user.name}</h2>
                <ul className="nav nav-tabs justify-content-center">
                    <li className="nav-item">
                        <span onClick={(e) => this._handleTabChange(e, 'edit-profile')} className={`nav-link ${this.state.editMode === 'profile' ? "active" : ""} span-app`}>Edit Profil</span>
                    </li>
                    <li className="nav-item">
                        <span onClick={(e) => this._handleTabChange(e, 'kategori')} className={`nav-link ${this.state.editMode === 'kategori' ? "active" : ""} span-app`}>Kategori Favorit</span>
                    </li>
                    <li onClick={(e) => this._handleTabChange(e, 'change-photo')} className="nav-item">
                        <span className={`nav-link ${this.state.editMode === 'photo' ? "active" : ""} span-app`}>Ubah Foto</span>
                    </li>
                    <li onClick={(e) => this._handleTabChange(e, 'change-password')} className="nav-item">
                        <span className={`nav-link ${this.state.editMode === 'password' ? "active" : ""} span-app`}>Ganti Password</span>
                    </li>
                </ul>

                {this.state.editMode === 'profile' && <UpdateProfile />}
                {this.state.editMode === 'kategori' && <UpdateCategory />}
                {this.state.editMode === 'photo' && <UpdatePhoto />}
                {this.state.editMode === 'password' && <ChangePassword />}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        user: state.user,
    }
}

export default connect(mapStateToProps, null)(Profile)
import React, { Component } from 'react'
import { connect } from 'react-redux'
import SimpleModal from '../modal/SimpleModal'

class UpdatePhoto extends Component {
    constructor(props) {
        super(props)
        this._handlePhotoUpload = this._handlePhotoUpload.bind(this)
    }

    async _handlePhotoUpload(event) {
        event.preventDefault()

        const fileInput = document.querySelector('#input-photo')
        const formData = new FormData()
        formData.append('photo', fileInput.files[0])

        var myHeaders = new Headers()
        myHeaders.append("Authorization", this.props.user.token)

        const requestOptions = {
            method: 'PUT',
            headers: myHeaders,
            body: formData,
        }

        const response = await fetch("http://kelompok5.dtstakelompok1.com/account/updatephoto", requestOptions)
            .then(resp => resp.json())
            .catch(error => console.log("Error: ", error))
        if (response == null) return null
        if (response.success) {
            this.props.dispatch({
                type: "CHANGE_PHOTO",
                payload: {
                    photo: response.account.photo,
                }
            })
            window.$('#photoUpdateModal').modal('show')
            console.log(response.account.photo)
        } else {
            console.log("Error: ", response.error)
            alert(`Error: ${response.error}`)
        }
    }

    render() {
        return (
            <form onSubmit={this._handlePhotoUpload} className="form-profile">
                <label htmlFor="input-photo">Unggah foto Anda</label>
                <input type="file" className="form-control-file" id="input-photo" />
                <button onClick={this._handlePhotoUpload} className="btn btn-app mt-2 mr-2">Simpan</button>

                <SimpleModal modalID='photoUpdateModal' modalTitle="Sukses" modalBody="Foto profil Anda berhasil diperbarui" />
            </form>
        )
    }
}

function mapStateToProps(state) {
    return {
        user: state.user
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatch
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdatePhoto)
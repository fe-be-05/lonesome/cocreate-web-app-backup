import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './EmailVerification.css'

export default class EmailVerification extends Component {
    constructor(props) {
        super(props)
        this.state = {
            verifySuccess: false,
        }
    }
    componentDidMount() {
        const { user_token } = this.props.match.params
        this._verifyEmail(user_token)
    }

    async _verifyEmail(token) {
        const response = await fetch(`http://kelompok5.dtstakelompok1.com/verify/${token}`)
            .then(resp => resp.json())
            .catch(error => {
                console.log('Error: ', error)
                alert(`Error: ${error}`)
            })
        if (response == null) return null
        if (response.success) {
            this.setState({ verifySuccess: true })
        }
    }

    render() {
        if (!this.state.verifySuccess) return (<div className="container">sedang proses...</div>)
        return (
            <div className="container text-center verify-message">
                <h2 className="font-weight-bold">Sukses!</h2>
                <p>Alamat email Anda berhasil diverifikasi</p>
                <Link to="/" role="button" className="btn btn-small btn-error-home">Home</Link>
            </div>
        )
    }
}

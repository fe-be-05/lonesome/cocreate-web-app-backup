import React, { Component } from 'react'
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import GoogleLogin from 'react-google-login'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebook, faGoogle } from '@fortawesome/free-brands-svg-icons'
import SimpleModal from '../modal/SimpleModal'
import RegisterImage from '../../assets/image/registrasi-main.png'
import './Register.css'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isProcessing: false,
            email: "",
            password: "",
            passwordRepeat: "",
            passwordMatch: false,
            registrationSuccess: false,
        }

        this._handleEmail = this._handleEmail.bind(this)
        this._handlePassword = this._handlePassword.bind(this)
        this._handlePasswordRepeat = this._handlePasswordRepeat.bind(this)
        this._handleRegister = this._handleRegister.bind(this)
    }

    _handleEmail(event) {
        this.setState({ email: event.target.value })
    }

    _handlePassword(event) {
        this.setState({ password: event.target.value })
    }

    _handlePasswordRepeat(event) {
        this.setState({ passwordRepeat: event.target.value })
    }

    async _handleRegister(event) {
        event.preventDefault()
        if (this.state.email.length === 0 || this.state.password === 0) return null

        this.setState({ isProcessing: true })

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({ "name": "", "email": this.state.email, "password": this.state.password });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        const response = await fetch("http://kelompok5.dtstakelompok1.com/register", requestOptions)
            .then(response => response.json())
            .catch(error => {
                alert('Error: ', error)
            });
        if (response == null) return null
        if (response.success) {
            this.setState({ registrationSuccess: true })
        } else {
            alert(`Error: ${response.error}`)
            console.log(response)
        }
        this.setState({ isProcessing: false })
        window.$('#registrasiModal').modal('show')
    }

    async _handleGoogleLogin(response) {

    }

    async _handleFacebookLogin(response) {

    }

    render() {
        return (
            <div className="container">
                <div class={`alert alert-info ${this.state.registrationSuccess ? "" : "d-none"}`} role="alert">Pedaftaran berhasil! Silakan cek email Anda!</div>
                <div className="row">
                    <div className="col-md-6 d-none d-sm-block">
                        <img className="registrasi-image" alt="ilustrasi-registrasi" src={RegisterImage} />
                    </div>
                    <div className="col-md-6">
                        <div class="card rounded shadow">
                            <div class="card-body">
                                <form className="form-login" onSubmit={this._handleRegister}>
                                    <h2 className="my-3">Silakan Daftar</h2>
                                    <div class="form-group">
                                        <label htmlFor="email">Email</label>
                                        <input required onChange={this._handleEmail} type="email" class="form-control" id="email" />
                                    </div>
                                    <div class="form-group">
                                        <label htmlFor="password">Password</label>
                                        <input required onChange={this._handlePassword} type="password" class="form-control" id="password" />
                                    </div>
                                    <div class="form-group">
                                        <label htmlFor="password-repeat">Ulangi Password</label>
                                        <input required onChange={this._handlePasswordRepeat} type="password" class="form-control" id="password-repeat" />
                                        <small className={`password-match-error ${this.state.password === this.state.passwordRepeat ? "d-none" : ""}`}>Password tidak sama</small>
                                    </div>
                                    <button onClick={this._handleRegister} class="btn btn-block btn-app mb-2" disabled={this.state.email.length === 0 || this.state.password.length <6 || (this.state.password !== this.state.passwordRepeat)}>{this.state.isProcessing ? 'processing...' : 'Daftar'}</button>
                                    <p>Sudah punya akun? <Link className="a-app" to="/login">Login</Link></p>
                                    <hr className="hr-login" />
                                </form>
                                <div className="social-login">
                                    <h5>atau login dengan</h5>
                                    <GoogleLogin
                                        clientId="251751587800-rh1fg90aj7pffllmmba33msir32376lu.apps.googleusercontent.com"
                                        render={renderProps => (
                                            <button className="btn btn-outline-dark" onClick={renderProps.onClick} ><FontAwesomeIcon icon={faGoogle} /> Google</button>
                                        )}
                                        buttonText="Login"
                                        onSuccess={this._handleGoogleLogin}
                                        onFailure={this._handleGoogleLogin}
                                        cookiePolicy={'single_host_origin'}
                                    />
                                    <FacebookLogin
                                        appId="655749328443697"
                                        callback={this._handleFacebookLogin}
                                        render={renderProps => (
                                            <button className="btn btn-outline-dark" onClick={renderProps.onClick}><FontAwesomeIcon icon={faFacebook} /> Facebook</button>
                                        )}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <SimpleModal modalID='registrasiModal' modalTitle='Registrasi Sukses!' modalBody='Silakan cek email Anda' />
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => {
    return {
        dispatch
    }
}

export default connect(null, mapDispatchToProps)(Register)
import React, { Component } from 'react'
import SplashScreen from '../loading'
import ArticleCreateForm from './ArticleCreateForm'

export default class ArticleEdit extends Component {
    constructor(props) {
        super(props)
        this.state = {
            hasHitAPI: false,
            article: {},
            articleFound: true,
        }
    }

    componentDidMount() {
        this._loadArticle(this.props.match.params.article_id)
    }

    async _loadArticle(id) {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow',
        };

        const response = await fetch(`http://kelompok5a.dtstakelompok1.com/article/get-articles/${id}`, requestOptions)
            .then(response => response.json())
            .catch(error => {
                console.log('Error: ', error)
                alert(`Error: ${error}`)
            });

        if (response == null) {
            this.setState({ articleFound: false })
            return null
        }

        if (response.success) {
            this.setState({
                article: response.articles,
                articleFound: true,
            })
            console.log(this.state.article.comment)
        } else {
            this.setState({ articleFound: false })
            console.log('Error: ', response.error)
            alert(`Error: ${response.error}`)
        }
        this.setState({ hasHitAPI: true })
    }

    render() {
        if (!this.state.hasHitAPI) return <SplashScreen />
        return (
            <div className="container">
                <ArticleCreateForm editMode={true} article={this.state.article}/>
            </div>
        )
    }
}

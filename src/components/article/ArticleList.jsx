import React, { Component, Fragment } from 'react'
import { Link } from 'react-router-dom'
import LoadingScreen from '../loading/index'
import './ArticleList.css'
import slugify from '../../helper/slugify'
import humanDate from '../../helper/humanDate'

export default class ArticleList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            articles: [],
            articlesHtml: [], // respon artikel2 yang sudah diubah ke html
            hasHitAPI: false,
        }
    }

    componentDidMount() {
        this._getArticles()
    }

    async _getArticles() {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        };

        const response = await fetch("http://kelompok5a.dtstakelompok1.com/article/get-articles", requestOptions)
            .then(response => response.json())
            .catch(error => {
                console.log('Error: ', error)
                alert(`Error: ${error}`)
            });
        if (response == null) return null
        if (response.success) {
            this.setState({
                articles: response.articles,
                articlesHtml: this._mapArticlesToHtml(response.articles),
                hasHitAPI: true,
            })
        } else {
            console.log('Error: ', response.error)
        }
    }

    _mapArticlesToHtml(ary) {
        const articles = ary.map((item) => {
            return (
                <div className="article row">
                    <div className="col-md-3">
                        {item.photo && <img alt="artikel header" src={item.photo} className="img-header" />}
                    </div>
                    <div className="col-md-9">
                        <Link className="a-app" to={`/article/${item.id}/${slugify(item.title)}`}><h3>{item.title}</h3></Link>
                        <p>{window.$(`<div>${item.description}</div>`).text().substring(0, 300) + "..."}</p>
                        <p className="text-right"><small>by <Link to={`/user/detail/${item.profile_id}`} className="a-app"><b>{item.author}</b></Link> at {humanDate(item.created_at)}</small></p>
                    </div>
                </div>
            )
        })
        return articles.reverse()
    }

    render() {
        return (
            !this.state.hasHitAPI ? <LoadingScreen /> : (
                <Fragment>
                    <h3>Artikel Terbaru</h3>
                    {this.state.articlesHtml}
                </Fragment>
            )
        )
    }
}

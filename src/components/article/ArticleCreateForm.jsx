import React, { Component } from 'react'
import { Editor } from '@tinymce/tinymce-react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import slugify from '../../helper/slugify'

class ArticleCreateForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            title: this.props.editMode ? this.props.article.title : "",
            editor: this.props.editMode ? this.props.article.description : "",
            availableCategories: [], // semua kategori yang tersedia
            selectedCategory: this.props.editMode? this.props.article.categories_id : "", // kategori artikel ini yang dipilih user
            articleCreated: false, // artikel sukses masuk DB
            createdArticleID: "", // ID artikel ini, diambil dari server jika POST berhasil
        }
        this._handleEditorChange = this._handleEditorChange.bind(this)
        this._handleFormSubmit = this._handleFormSubmit.bind(this)
        this._renderCategories = this._renderCategories.bind(this)
    }

    componentDidMount() {
        this._getAllCategories()
    }

    async _getAllCategories() {
        const response = await fetch('http://kelompok5.dtstakelompok1.com/category')
            .then(resp => resp.json())
            .catch(error => {
                console.log('Error: ', error)
                alert(`Error: ${error}`)
            })
        if (response == null) return null
        if (response.success) {
            var categories = []
            response.account.forEach((item) => categories.push({ id: item.id, name: item.category_name }))
            // masukkan kategori2 yang ada ke state array
            this.setState({ availableCategories: categories })
        }
    }

    _renderCategories() {
        const list = this.state.availableCategories.map((item) => {
            return (
                <div class="form-check">
                    <input onChange={(e) => this.setState({ selectedCategory: e.target.value })} className="form-check-input" type="radio" name={item.name} id={`category-${item.id}`} value={item.id} checked={this.state.selectedCategory.toString() === item.id.toString()} />
                    <label className="form-check-label" htmlFor={`category-${item.id}`}>{item.name}</label>
                </div>
            )
        })
        return list
    }

    _handleEditorChange(value) {
        this.setState({ editor: value })
    }

    async _handleFormSubmit(event) {
        event.preventDefault()

        const fileInput = document.querySelector('#input-header-photo')
        const formData = new FormData()
        formData.append('title', this.state.title)
        formData.append('photo', fileInput.files[0])
        formData.append('description', this.state.editor)
        formData.append('categories_id', this.state.selectedCategory)
        //formData.append('profile_id', this.props.user.id)

        var myHeaders = new Headers()
        myHeaders.append("Authorization", this.props.user.token)

        const requestOptions = {
            method: this.props.editMode? 'PUT' : 'POST',
            headers: myHeaders,
            body: formData,
            redirect: "follow",
        }

        const url = this.props.editMode ? `http://kelompok5a.dtstakelompok1.com/article/update-articles/${this.props.article.id}` : 'http://kelompok5a.dtstakelompok1.com/article/create-articles'
        const response = await fetch(url, requestOptions)
            .then(resp => resp.json())
            .catch(error => {
                console.log('Error: ', error)
                alert(`Error: ${error}`)
            })
        if (response == null) return null
        if (response.success) {
            this.setState({ articleCreated: true, createdArticleID: response.articles.id }) // untuk redirect
        } else {
            console.log('Error', response.error)
            alert(`Error: ${response.error}`)
        }
    }

    render() {
        if (this.state.articleCreated) return <Redirect to={`/article/${this.state.createdArticleID}/${slugify(this.state.title)}`} />
        return (
            <div>
                <form onSubmit={this._handleFormSubmit}>
                    <h3>Tulis Artikel Baru</h3>
                    <div className="form-group">
                        <label htmlFor="input-title">Judul:</label>
                        <input onChange={(e) => this.setState({ title: e.target.value })} type="text" id="input-title" className="form-control" value={this.state.title} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="input-header-photo">Gambar Header:</label>
                        <input type="file" id="input-header-photo" className="form-control" />
                    </div>
                    <Editor
                        initialValue={this.state.editor}
                        apiKey='1zb87ndi27om0m3ageb55s5muhd43su0zxxf7nhqy558k3q0'
                        init={{
                            height: 300,
                            menubar: false,
                            plugins: [
                                'advlist autolink lists link image charmap print preview anchor',
                                'searchreplace visualblocks code fullscreen',
                                'insertdatetime media table paste code help wordcount'
                            ],
                            toolbar:
                                'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
                        }}
                        onEditorChange={this._handleEditorChange}
                    />
                    <div className="form-group mt-3">
                        Kategori:
                        {this._renderCategories()}
                    </div>
                    <button onClick={this._handleFormSubmit} className="btn btn-block btn-primary my-3" disabled={this.state.title.length === 0 || this.state.editor.length === 0 || this.state.selectedCategory.length === 0}>{this.props.editMode ? "Update" : "Submit"}</button>
                </form>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
    }
}

export default connect(mapStateToProps, null)(ArticleCreateForm)
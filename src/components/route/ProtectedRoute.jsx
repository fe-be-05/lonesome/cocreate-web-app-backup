import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Redirect, Route } from 'react-router-dom'

class ProtectedRoute extends Component {
    render() {
        const route = this.props.component ?
            (<Route exact={this.props.exact} path={this.props.path} component={this.props.component} />) :
            (
                <Route exact={this.props.exact} path={this.props.path}>
                    {this.props.children}
                </Route>
            )

        if (this.props.adminOnly && this.props.userIsAdmin) {
            return route
        } else if (this.props.adminOnly && !this.props.userIsAdmin) {
            return <Redirect to='/' />
        }
        else if (!this.props.adminOnly && this.props.userIsLoggedIn) {
            return route
        } else {
            return <Redirect to='/' />
        }
    }
}

const mapStateToProps = state => {
    return {
        userIsLoggedIn: state.userIsLoggedIn,
        userIsAdmin: state.user.isadmin
    }
}

export default connect(mapStateToProps, null)(ProtectedRoute)
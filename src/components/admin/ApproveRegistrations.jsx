import React, { Component } from 'react'
import { connect } from 'react-redux'
import "./ApproveRegistrations.css"

class ApproveRegistrations extends Component {
    constructor(props) {
        super(props)
        this.state = {
            accounts: [],
        }
    }

    // Query calon user yang bisa di approve
    async _getUsers() {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");
        myHeaders.append("Authorization", this.props.user.token);

        var requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        const response = await fetch("http://kelompok5.dtstakelompok1.com/admin/account", requestOptions)
            .then(response => response.json())
            .catch(error => {
                console.log('Error: ', error)
                alert(`Alert: ${error}`)
            });
        if (response == null) return null

        if (response.success) {
            // user yang belum di-approve atau reject dimasukkan ke state
            this.setState({
                accounts: response.account.filter(item => !item.isapproved && !item.isdeleted)
            })
            console.log(this.state.accounts)
        } else {
            console.log('Error: ', response.error)
            alert(`Error: ${response.error}`)
        }
    }

    componentDidMount() {
        this._getUsers()
    }

    async _approveUser(id) {
        var url = `http://kelompok5.dtstakelompok1.com/admin/account/register/${id}`
        console.log(url)
        var myHeaders = new Headers();
        myHeaders.append("Authorization", this.props.user.token);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            redirect: 'follow',
        };

        await fetch(url, requestOptions)
            .then(response => response.json())
            .then(result => console.log(result))
            .catch(error => console.log('Error: ', error));

        await this._getUsers()
    }


    async _rejectUser(id) {
        var url = `http://kelompok5.dtstakelompok1.com/admin/account/delete/${id}`
        console.log(url)
        var myHeaders = new Headers();
        myHeaders.append("Authorization", this.props.user.token);

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            redirect: 'follow',
        };

        await fetch(url, requestOptions)
            .then(response => response.json())
            .then(result => console.log(result))
            .catch(error => console.log('error', error));

        await this._getUsers()
    }

    render() {
        return (
            <div className="container">
                <h2>Daftar Calon Member</h2>
                <button onClick={() => this._getUsers()} className="btn btn-light my-1">Refresh</button>
                <div className="div-table">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Email</th>
                                <th scope="col">Sudah verifikasi email?</th>
                                <th scope="col">Tanggal daftar</th>
                                <th scope="col">Tindakan</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.accounts.length === 0 && "Tidak ada data"}
                            {this.state.accounts.map((value, index) =>
                                <tr>
                                    <th scope="row">{index + 1}</th>
                                    <td>{value.name}</td>
                                    <td>{value.email}</td>
                                    <td>{value.emailverification ? "Sudah" : "Belum"}</td>
                                    <td>{value.created_at.substring(0, 10)}</td>
                                    <td>
                                        <button onClick={async () => { this._approveUser(value.ID) }} className="btn btn-sm btn-primary mr-2">Terima</button>
                                        <button onClick={async () => { this._rejectUser(value.ID) }} className="btn btn-sm btn-danger">Tolak</button>
                                    </td>
                                </tr>)}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

const _mapStatetoProps = state => {
    return {
        userIsLoggedIn: state.userIsLoggedIn,
        user: state.user
    }
}

const _mapDispatchToProps = dispatch => {
    return {
        dispatch
    }
}

export default connect(_mapStatetoProps, _mapDispatchToProps)(ApproveRegistrations)
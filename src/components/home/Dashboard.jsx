import React, { Component } from 'react'
import { connect } from 'react-redux'
import ArticleList from '../article/ArticleList'
import './Dashboard.css'
import DefaultProfileImage from '../../assets/image/profile.png'

class Dashboard extends Component {
    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-9 main-dashboard">
                        <ArticleList />
                    </div>
                    <div className="col-md-3">
                        <img className="rounded-circle dashboard-profile-photo" src={this.props.user.photo || DefaultProfileImage} alt="profil" />
                        <h5 className="text-center">Halo, {this.props.user.name || this.props.user.email}</h5>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        user: state.user,
    }
}

export default connect(mapStateToProps, null)(Dashboard)
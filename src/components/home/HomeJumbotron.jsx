import React, { Component, Fragment } from 'react'
import LazyLoad from 'react-lazyload'
import HomeImage from '../../assets/image/flat-design-home.png'
import { Link } from 'react-router-dom'
import ArticleList from '../article/ArticleList'
import './HomeJumbotron.css'
import { faGitlab, faJira } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default class HomeJumbotron extends Component {
    render() {
        return (
            <Fragment>
                <div className="jumbotron jumbotron-fluid jumbotron-home">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6">
                                <h1 className="display-4">Kolaborasi dan inovasi menjadi satu</h1>
                                <p className="lead">Gabung dan jadilah bagian dari komunitas ini</p>
                                <Link to="/register" className="btn btn-jumbotron">Yuk Gabung!</Link>
                            </div>
                            <div className="col-md-6 d-none d-sm-block">
                                <LazyLoad height={480}>
                                    <img alt="home" src={HomeImage} className="img-home" />
                                </LazyLoad>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="about-us text-center" id="about-us">
                    <h2 className="mb-5"><strong>Tentang kami</strong></h2>
                    <p>CoCreate merupakan platform digital komunitas bagi tim banking dan terbesar di Indonesia. Tagline dari CoCreate yakni kolaborasi dan inovasi</p>
                </div>
                <div className="fitur text-center px-5" id="fitur">
                    <h2 className="mb-5"><strong>Fitur</strong></h2>
                    <div className="row">
                        <div className="col-md-4">
                            <h3>Kolaborasi CoCreate</h3>
                            <p>Anggota CoCreate dapat membuat artikel baru tentang new idea/insight, berita, bahkan proyek inovasi dengan kategori tertentu dengan membuat grup Proyek Inovasi (PI).</p>
                        </div>
                        <div className="col-md-4">
                            <h3>Portfolio Anggota CoCreate</h3>
                            <p>Anggota CoCreate dapat melihat kontribusi/portfolio anggota lain, menambahkan dan mengubah daftar proyek external, pengetahuan/edukasi/expertise/knowledges, dan pengalaman di portfolio mereka, serta mengunduh portofolio sebagai CV/Resume digital.</p>
                        </div>
                        <div className="col-md-4">
                            <h3>CoCreate Innovation Showcases</h3>
                            <p>Proyek inovasi yang telah dibuat bisa dipamerkan menggunakan fitur CoCreate Innovation Showcases.</p>
                        </div>
                    </div>
                </div>
                <div className="container" id="discover">
                    <ArticleList />
                </div>
                <div className="our-team text-center" id="our-team">
                    <h2>Our Team</h2>
                    <div className="row">
                        <div className="col-md-6">
                            <h3>Front End</h3>
                            <p>Aisyah Nurul Hidayah</p>
                            <p>Gottfried Christophorus Prasetyadi N.</p>
                            <p>Muhammad Aswar Bakri</p>
                            <p>Ryan Apriansyah</p>
                        </div>
                        <div className="col-md-6">
                            <h3>Back End</h3>
                            <p>Muhammad Reza Bintami</p>
                            <p>Jupri Eka Pratama</p>
                            <p>Muhammad Zikri</p>
                            <p>Esteria Novebri Simanjuntak</p>
                        </div>
                    </div>
                    <Link className="a-app mx-3" to="https://gitlab.com/fe-be-05/cocreate-web-app"><FontAwesomeIcon icon={faGitlab} /></Link>
                    <Link className="a-app mx-3" to="https://aisyahnrlh.atlassian.net"><FontAwesomeIcon icon={faJira} /></Link>
                </div>
            </Fragment>
        )
    }
}

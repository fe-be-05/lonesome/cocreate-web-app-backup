import React, { Component } from 'react'
import SimpleModal from '../modal/SimpleModal'
import './ResetPassword.css'

export default class ResetPassword extends Component {
    constructor(props) {
        super(props)
        this.state = {
            password: "",
            password2: "",
            resetSuccess: false,
        }

        this._handleResetPassword = this._handleResetPassword.bind(this)
    }

    async _handleResetPassword(event) {
        event.preventDefault()

        const { user_token } = this.props.match.params

        const response = await fetch(`http://kelompok5.dtstakelompok1.com/reset-password/${user_token}`, {method: 'POST', body: JSON.stringify({"new_password": this.state.password})})
            .then(resp => resp.json())
            .catch(e => {
                console.log('Error: ', e)
                alert(`Error: ${e}`)
            })
        if (response == null) return null
        if (response.success) {
            window.$("#modalResetPassword").modal('show')
        } else {
            console.log('Error: ', response.error)
            window.$("#modalResetPasswordFailed").modal('show')
        }
    }

    render() {
        return (
            <div className="container reset-password">
                <form onSubmit={this._handleResetPassword}>
                    <h2 className="my-3">Reset Password Anda</h2>
                    <div className="form-group">
                        <label htmlFor="input-password">Password baru:</label>
                        <input type="password" onChange={(e) => this.setState({ password: e.target.value })} id="input-password" className="form-control" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="input-password2">Ulangi password:</label>
                        <input type="password" onChange={(e) => this.setState({ password2: e.target.value })} id="input-password2" className="form-control" />
                    </div>
                    <button onClick={this._handleResetPassword} className="btn btn-app" disabled={this.state.password !== this.state.password2 || this.state.password.length === 0}>Reset Password</button>
                </form>

                <SimpleModal modalID="modalResetPassword" modalTitle="Sukses!" modalBody="Password Anda telah berhasil di-reset. Silakan login." />
                <SimpleModal modalID="modalResetPasswordFailed" modalTitle="Gagal" modalBody="Terjadi kesalahan" />
            </div>
        )
    }
}

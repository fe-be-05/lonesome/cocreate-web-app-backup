import loginImage from '../../assets/image/login-main.png'
import './Login.css'
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link, Redirect } from 'react-router-dom'
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import GoogleLogin from 'react-google-login'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebook, faGoogle } from '@fortawesome/free-brands-svg-icons'
import { reactLocalStorage } from 'reactjs-localstorage';
import SimpleModal from '../modal/SimpleModal'

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            rememberMe: false,
            isProcessing: false,
            loginAsAdmin: false,
            emailForForgotPassword: "",
        }

        this._handleEmail = this._handleEmail.bind(this)
        this._handlePassword = this._handlePassword.bind(this)
        this._handleCheckbox = this._handleCheckbox.bind(this)
        this._handleLogin = this._handleLogin.bind(this)
        this._handleForgotPassword = this._handleForgotPassword.bind(this)
    }

    _handleEmail(event) {
        this.setState({ email: event.target.value })
    }

    _handlePassword(event) {
        this.setState({ password: event.target.value })
    }

    _handleCheckbox(event) {
        this.setState({ loginAsAdmin: event.target.checked })
    }

    async _handleLogin(event) {
        event.preventDefault()
        // tidak usah respon kalau kedua field masih kosong
        if (this.state.email.length === 0 || this.state.password.length === 0) return null

        this.setState({ isProcessing: !this.state.isProcessing })

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({ "email": this.state.email, "password": this.state.password });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        var url = this.state.loginAsAdmin ? "http://kelompok5.dtstakelompok1.com/admin/login" : "http://kelompok5.dtstakelompok1.com/login"
        var response = await fetch(url, requestOptions)
            .then(response => response.json())
            .catch(error => {
                console.log('Error: ', error)
                alert(`Error: ${error}`)
            });
        if (response == null) return null
        if (response.success) {
            // panggil api profile
            var token = response.token
            myHeaders.append("Authorization", token)
            requestOptions = {
                method: 'GET',
                headers: myHeaders,
                redirect: 'follow',
            }
            url = this.state.loginAsAdmin ? "http://kelompok5.dtstakelompok1.com/admin" : "http://kelompok5.dtstakelompok1.com/account"
            response = await fetch(url, requestOptions)
                .then(response => response.json())
                .catch(error => alert(`Error: ${error}`))
            if (response == null) return null
            if (response.success) {
                console.log(`logged in as ${response.account.email}`)
                const user = response.account
                user.token = token
                // set global state
                this.props.dispatch({
                    type: "LOGIN",
                    payload: {
                        user,
                    }
                })
                // also save to localstorage if remeber me
                if (this.state.rememberMe) {
                    reactLocalStorage.setObject('user', user)
                    reactLocalStorage.set('rememberMe', this.state.rememberMe)
                }
            } else {
                alert(`Error: ${response.error}`)
            }
        } else {
            alert(`Error: ${response.error}`)
        }

        this.setState({ isProcessing: !this.state.isProcessing })
    }

    _handleGoogleLogin() {

    }

    _handleFacebookLogin() {

    }

    async _handleForgotPassword(event) {
        event.preventDefault()

        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({ "email": this.state.emailForForgotPassword });

        var requestOptions = {
            method: 'POST',
            headers: myHeaders,
            body: raw,
            redirect: 'follow'
        };

        const response = await fetch("http://kelompok5.dtstakelompok1.com/forgot-password", requestOptions)
            .then(response => response.json())
            .catch(error => {
                console.log('Error: ', error)
                alert(`Error: ${error}`)
            })
        if (response == null) return null
        if (response.success) {
            window.$('#modalForgotPassword').modal('hide')
            window.$('#modalForgotPasswordEmailSent').modal('show')
        }
    }

    // tampilkan form lupa password dg field email
    _showForgotPasswordForm() {
        return (
            <form onSubmit={this._handleForgotPassword}>
                <div className="form-group">
                    <label htmlFor="input-email">Email:</label>
                    <input onChange={(e) => this.setState({ emailForForgotPassword: e.target.value })} type="email" placeholder="Masukkan alamat email Anda disini" className="form-control" id="input-email" />
                </div>
                <button onClick={this._handleForgotPassword} className="btn btn-primary" disabled={this.state.emailForForgotPassword.length === 0}>Kirim</button>
            </form>
        )
    }

    render() {
        // jika sudah login, redirect ke home
        if (this.props.userIsLoggedIn) return <Redirect to='/' />
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-6 d-none d-sm-block">
                        <img className="login-image" alt="login illustration" src={loginImage} />
                    </div>
                    <div className="col-md-6">
                        <div class="card rounded shadow">
                            <div class="card-body">
                                <form className="form-login" onSubmit={this._handleLogin}>
                                    <h2 className="my-3">Silakan Login</h2>
                                    <div class="form-group">
                                        <label htmlFor="email">Email</label>
                                        <input required onChange={this._handleEmail} type="email" class="form-control" id="email" />
                                    </div>
                                    <div class="form-group">
                                        <label htmlFor="password">Password</label>
                                        <input required onChange={this._handlePassword} type="password" class="form-control" id="password" />
                                    </div>
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input" id="remember-me" checked={this.state.rememberMe} onChange={() => this.setState({ rememberMe: !this.state.rememberMe })} />
                                        <label class="form-check-label" htmlFor="remember-me">Ingat saya</label>
                                    </div>
                                    <div class="form-group form-check">
                                        <input type="checkbox" class="form-check-input" id="checkbox-admin" onChange={this._handleCheckbox} />
                                        <label class="form-check-label" htmlFor="checkbox-admin">Login sebagai admin</label>
                                    </div>
                                    <button onClick={this._handleLogin} type="submit" class="btn btn-block btn-app mb-2">{this.state.isProcessing ? 'processing...' : `Login${this.state.loginAsAdmin ? " sebagai Admin" : ""}`}</button>
                                    <p>Belum punya akun? <Link to="/register" className="a-app">Daftar</Link></p>
                                    <p><Link onClick={() => { window.$('#modalForgotPassword').modal('show') }} className="a-app">Lupa password?</Link></p>
                                    <hr className="hr-login" />
                                </form>
                                <div className="social-login">
                                    <h5>atau login dengan</h5>
                                    <GoogleLogin
                                        clientId="251751587800-rh1fg90aj7pffllmmba33msir32376lu.apps.googleusercontent.com"
                                        render={renderProps => (
                                            <button className="btn btn-outline-dark" onClick={renderProps.onClick} ><FontAwesomeIcon icon={faGoogle} /> Google</button>
                                        )}
                                        buttonText="Login"
                                        onSuccess={this._handleGoogleLogin}
                                        onFailure={this._handleGoogleLogin}
                                        cookiePolicy={'single_host_origin'}
                                    />
                                    <FacebookLogin
                                        appId="655749328443697"
                                        callback={this._handleFacebookLogin}
                                        render={renderProps => (
                                            <button className="btn btn-outline-dark" onClick={renderProps.onClick}><FontAwesomeIcon icon={faFacebook} /> Facebook</button>
                                        )}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {/* Modal untuk lupa password */}
                <SimpleModal modalID="modalForgotPassword" modalTitle="Lupa Password?" modalBody={this._showForgotPasswordForm()} />
                <SimpleModal modalID="modalForgotPasswordEmailSent" modalTitle="Sukses!" modalBody="Cek email Anda untuk instruksi selanjutnya." />
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        userIsLoggedIn: state.userIsLoggedIn
    }
}

const mapDispatchToProps = dispatch => {
    return {
        dispatch
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
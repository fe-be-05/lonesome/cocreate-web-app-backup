import { createStore } from 'redux'

// tambah/modif initial state untuk app reactjs ini disini
const initialState = {
    userIsLoggedIn: false,
    user: {},
}

// berbagai action type yang tersedia di app ini:
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'LOGIN':
            return {
                ...state,
                userIsLoggedIn: true,
                user: action.payload.user,
            }
        case 'LOGOUT':
            return {
                ...state,
                userIsLoggedIn: false,
                user: {},
            }
        case 'CHANGE_PHOTO':
            return {
                ...state,
                user: {
                    ...state.user,
                    photo: action.payload.photo,
                }
            }
        default:
            return state
    }
}

const store = createStore(
    reducer,
    // untuk debugger pada web browser
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

export default store
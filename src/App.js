import React, { Component } from 'react'
import './App.css';
import {
  HashRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import Home from './components/home/Home'
import Login from './components/login/Login'
import Navbar from './components/navbar/Navbar'
import NoMatch from './components/error/Error404'
import Register from './components/register/Register';
import ProtectedRoute from './components/route/ProtectedRoute'
import ApproveRegistrations from './components/admin/ApproveRegistrations'
import Profile from './components/user/Profile'
import Article from './components/article/Article'
import ArticleCreate from './components/article/ArticleCreate';
import EmailVerification from './components/register/EmailVerification';
import ResetPassword from './components/login/ResetPassword';
import ProfileView from './components/user/ProfileView'
import { connect } from 'react-redux';
import { reactLocalStorage } from 'reactjs-localstorage';
import ArticleEdit from './components/article/ArticleEdit';


class App extends Component {
  constructor(props) {
    super(props)
    // check if last user wanted to be remembered
    if (reactLocalStorage.get('rememberMe', false) && !this.props.userIsLoggedIn) {
      const user = reactLocalStorage.getObject('user')
      if (user) {
        this.props.dispatch({
          type: "LOGIN",
          payload: {
            user,
          }
        })
      }
    }
  }

  componentDidMount() {

  }

  render() {
    return (
      <div className="app">
        <Router>
          <Navbar />
          <Switch>
            <Route exact path="/login">
              <Login />
            </Route>
            <Route exact path="/register">
              <Register />
            </Route>
            <Route exact path="/verify/:user_token" component={EmailVerification} />
            <Route exact path="/reset-password/:user_token" component={ResetPassword} />

            <ProtectedRoute exact path="/article/:article_id/:slug/edit" component={ArticleEdit} />
            <Route exact path='/article/:article_id/:slug' component={Article} />
            <ProtectedRoute exact path="/user/profile">
              <Profile />
            </ProtectedRoute>
            <Route exact path="/user/detail/:user_id" component={ProfileView} />
            <ProtectedRoute exact path="/user/create-article">
              <ArticleCreate />
            </ProtectedRoute>
            <ProtectedRoute adminOnly exact path="/admin/approve-registrations">
              <ApproveRegistrations />
            </ProtectedRoute>

            <Route exact path="/">
              <Home />
            </Route>
            <Route path='*'>
              <NoMatch />
            </Route>
          </Switch>

          <div className="footer">
            &copy; 2020 UI-05
        </div>
        </Router>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    userIsLoggedIn: state.userIsLoggedIn,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatch
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)